# [Network Speed Bot](https://twitter.com/networkspeedbot/with_replies)
[![N|Solid](https://cdn2.iconfinder.com/data/icons/minimalism/128/twitter.png)](https://twitter.com/networkspeedbot/with_replies)

## Installation (for Raspberry Pi)
#### 1. Update & upgrade
```sh
sudo apt-get update
sudo apt-get upgrade
sudo apt-get dist-upgrade
sudo apt-get clean
```

#### 2. Install Miniconda3
Source: http://stackoverflow.com/questions/39371772/how-to-install-anaconda-on-raspberry-pi-3-model-b

1. Get the latest version of miniconda for Raspberry pi - made for armv7l processor and bundled with Python 3 (eg.: uname -m)
```sh
wget http://repo.continuum.io/miniconda/Miniconda3-latest-Linux-armv7l.sh
sudo md5sum Miniconda3-latest-Linux-armv7l.sh
sudo /bin/bash Miniconda3-latest-Linux-armv7l.sh
```

2. When installing change the default dir installation from /root/miniconda3 to: ```/home/pi/miniconda3```

3. Edit the .bashrc file
```sudo nano /home/pi/.bashrc```
...and add the following line to the end of the file
```export PATH="/home/pi/miniconda3/bin:$PATH"```

4. Save and close this file & reboot Raspberry pi
```sudo reboot -h now```

5. After reboot enter the following command "python --version" which should give you:
```Python 3.4.3 :: Continuum Analytics, Inc.```

6. Running ```which python``` should give:
```sh
/home/pi/miniconda3/bin/python
```


#### 3. Install pip
1. ```sudo -s```
2. ```conda install pip```
3. ```pip install --upgrade pip```

#### 4. Build Matplotlib
```sh
cd ~
git clone git://github.com/matplotlib/matplotlib.git
cd matplotlib
python setup.py install
```
After install is finished run
```rm -r ~/matplotlib```
to delete the matplotlib directory

#### 5. Install pyspeedtest
```sudo -s```
```pip install pyspeedtest```

#### 6. Install TwitterAPI
```sudo -s```
```pip install TwitterAPI```

#### 7. Set up Network Speed Bot
```sh
cd ~/Documents
git clone https://gitlab.com/ashish28ranjan/network-speed-bot.git
cd network-speed-bot
touch debug.log
sudo chmod 666 debug.log
touch internet_speed.json
sudo chmod 666 internet_speed.json
```

## Running

#### 1. Open terminal and type
```nohup ~/miniconda3/bin/python ~/Documents/network-speed-bot/network_speed_bot.py &```
#### 2. Check if the python process started
```ps -ef | grep python```
#### 3. Close terminal
