import json, time, datetime, collections, pyspeedtest
import matplotlib.pyplot as plt
import numpy as np
from TwitterAPI import TwitterAPI
import logging

from twitterenv import *

queryDate = time.strftime("%d-%m-%Y")
qph = 4 # query per hour
startHour = 2
endHour = 12
limit = 16
limit_max = 20
totalTicks = 6
maxRetries = 30

intervalArray = [5, 10, 15, 20, 30, 40, 45, 60, 75, 90, 120, 135, 150, 180, 210, 240, 300, 360]

logging.basicConfig(filename='debug.log', format='%(asctime)s %(levelname)s %(message)s', level=logging.DEBUG)

def fivethirtyeight(x, x_ticks, y_d, y_u, y_p, y_l, y_max):
    plt.style.use('fivethirtyeight')
    fig, ax = plt.subplots()
    download, = ax.plot(x, y_d, color='#fc4f30', label="Download (Mbps)", linewidth=2)
    upload, = ax.plot(x, y_u, color='#008fd5', label="Upload (Mbps)", linewidth=2)
    # ping, = ax.plot(x, y_p, label="Ping (ms)", linewidth=2)
    limit, = ax.plot(x, y_l, color='#e5ae38', linewidth=2, linestyle='--')
    y_limit, = ax.plot(x, y_max, color='#e5ae38', linewidth=2, linestyle='--')
    
    interval = int(60*((x[len(x)-1] - x[0])/(totalTicks-1)))

    for i, val in enumerate(intervalArray):
        if (interval < val):
            break
    diff1 = abs(interval-intervalArray[i-1])
    diff2 = abs(interval-intervalArray[i])
    if (diff1 < diff2):
        nearestInterval = intervalArray[i-1]
    else:
        nearestInterval = intervalArray[i]

    startTimeFloat = x[0]
    xTicksArray = np.array([startTimeFloat])
    startTimeMinutes = int(startTimeFloat*60)
    i=1
    while(1):
        if (startTimeMinutes < nearestInterval*i):
            startTimeMinutes = nearestInterval*(i-1)
            break
        i += 1

    xTicksAlias = np.array(['{0:02.0f}:{1:02.0f}'.format(*divmod(startTimeMinutes, 60))])
    (h, m) = xTicksAlias[0].split(':')
    startTime = int(h) * 60 + int(m)

    for i in range(1,totalTicks):
        xTicksArray = np.append(xTicksArray, xTicksArray[i-1]+nearestInterval/60)
        temp = startTime + nearestInterval*i
        xTicksAlias = np.append(xTicksAlias, '{0:02.0f}:{1:02.0f}'.format(*divmod(temp, 60)))

    plt.xticks(xTicksArray, xTicksAlias)

    box = ax.get_position()
    ax.set_position([box.x0, box.y0 + box.height * 0.03, box.width, box.height * 0.9])

    ax.set_title("(source: speedtest.net)", fontsize=11, color='#277c31')
    ax.legend(loc='lower center', bbox_to_anchor=(0.5, 1.03), fancybox=True, shadow=True, ncol=2)
    ax.set_xlabel('Time (24 hour format) [' + queryDate + ']', fontsize=13)
    ax.set_ylabel('Speed (in Mbps)', fontsize=14)
    xCenter = (x[len(x)-1] + x[0])/2
    plt.annotate('16 Mbps', fontsize=13, color='#632337', xy=(xCenter, 16), xytext=(xCenter, 17), arrowprops=dict(facecolor='#632337', shrink=0.03))

    leg = plt.gca().get_legend()
    ltext  = leg.get_texts()  # all the text.Text instance in the legend
    plt.setp(ltext, fontsize=13)    # the legend text fontsize

    fig.savefig(queryDate+'.png', bbox_inches='tight')
    # plt.show()

while(1):
    queryDate = time.strftime("%d-%m-%Y")
    while(1):
        now = datetime.datetime.now()
        nextNow = datetime.datetime.now() + datetime.timedelta(minutes = int(60/qph))
        if (now.hour >= endHour):
            break
        print("now:", now)
        logging.info("{MY} now: " + str(now))
        try:
            file = open("internet_speed.json", "r")
            data = json.load(file)
        except ValueError: 
            data = {}
        except OSError as e:
            print(str(e))
            logging.error("{MY} "+str(e))
            exit()
        file.close()

        if queryDate not in data:
            data[queryDate] = {}
        print(data[queryDate])
        logging.info("{MY} "+str(data[queryDate]))

        try:
            st = pyspeedtest.SpeedTest()
            ds = round((st.download())/1000000, 2)
            us = round((st.upload())/1000000, 2)
            ps = int(st.ping())

            currentTime = time.strftime('%H:%M:%S')
            data[queryDate][currentTime] = {"d":ds, "u":us, "p":ps}
            print(data[queryDate][currentTime])
            logging.info("{MY} " + str(data[queryDate][currentTime]))

            with open('internet_speed.json', 'w') as outfile:  
                json.dump(data, outfile)

            delay = int(time.mktime(nextNow.timetuple()))-int(time.mktime(datetime.datetime.now().timetuple()))
            print("delay:", delay)
            logging.info("{MY} delay: " + str(delay))
            time.sleep(abs(delay-2))
        except KeyboardInterrupt:
            print("Keyboard interrupt!")
            logging.error("{MY} Keyboard interrupt!")
            exit()
        except:
            time.sleep(60)
    
    try:
        qFile = open("internet_speed.json", "r")
        data = json.load(qFile)
        if (queryDate in data):
            od = collections.OrderedDict(sorted(data[queryDate].items()))
            x = np.array([])
            x_ticks = np.array([])
            y_d = np.array([])
            y_u = np.array([])
            y_p = np.array([])
            y_l = np.array([])
            y_max = np.array([])
            for key, value in od.items():
                (h, m, s) = key.split(':')
                decimalTime = round((int(h) * 3600 + int(m) * 60 + int(s))/3600, 3)
                x = np.append(x, decimalTime)
                x_ticks = np.append(x_ticks, h+":"+m)

                y_d = np.append(y_d, value["d"])
                y_u = np.append(y_u, value["u"])
                y_p = np.append(y_p, value["p"])
                y_l = np.append(y_l, limit)
                y_max = np.append(y_max, limit_max)
            fivethirtyeight(x, x_ticks, y_d, y_u, y_p, y_l, y_max)
            avg_down = round(np.mean(y_d), 2)
            avg_up = round(np.mean(y_u), 2)
            avg_ping = int(np.mean(y_p))

            retries = 1
            while(1):
                try:
                    file = open("internet_speed.json", "r")
                    data = json.load(file)
                except ValueError: 
                    data = {}
                except OSError as e:
                    print(str(e))
                    logging.error("{MY} "+str(e))
                    exit()
                file.close()

                if 'twitter' not in data:
                    data['twitter']={}

                if queryDate not in data['twitter']:
                    data['twitter'][queryDate] = 0
                print("data['twitter']["+queryDate+"] = "+str(data['twitter'][queryDate]))
                logging.info("{MY} "+"data['twitter']["+queryDate+"] = "+str(data['twitter'][queryDate]))
                
                if (data["twitter"][queryDate] > 0):
                    break
                else:
                    try:
                        api = TwitterAPI(CONSUMER_KEY, CONSUMER_SECRET, ACCESS_TOKEN_KEY, ACCESS_TOKEN_SECRET)
                        file = open(queryDate+'.png', 'rb')
                        media_data = file.read()
                        text_status = "@Airtel_Presence Speed status for "+str(time.strftime("%d %b, %Y"))+"\nAvg Down: "+str(avg_down)+" Mbps\nAvg Up: "+str(avg_up)+" Mbps"
                        print(text_status)
                        logging.info("{MY} Twitter Status: "+text_status)
                        r = api.request('statuses/update_with_media', {'status':text_status}, {'media[]':media_data})
                        print(r.status_code)
                        logging.info("{MY} Twitter response code: " + str(r.status_code))
                        if (r.status_code==200 or retries>maxRetries):
                            data['twitter'][queryDate] = data['twitter'][queryDate] + 1
                            with open('internet_speed.json', 'w') as outfile:  
                                json.dump(data, outfile)
                            break
                        retries += 1
                        time.sleep(60)
                    except KeyboardInterrupt:
                        print("Keyboard interrupt!")
                        logging.error("{MY} Keyboard interrupt!")
                        exit()
                    except:
                        time.sleep(600)

    except OSError as e:
        print(str(e))
        logging.error("{MY} "+str(e))


    tomorrow = now + datetime.timedelta(days=1)
    stringTargetTime = str(tomorrow.day)+"-"+str(tomorrow.month)+"-"+str(tomorrow.year)+" "+str(startHour)+":00:00"
    targetTime = int(time.mktime(datetime.datetime.strptime(stringTargetTime, "%d-%m-%Y %H:%M:%S").timetuple()))
    currentTime = int(time.time())
    print(abs(targetTime-currentTime))
    logging.info("{MY} Next day delay: "+str(abs(targetTime-currentTime)))
    time.sleep(abs(targetTime-currentTime))
